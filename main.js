import React, {Component} from "react";
import update from "immutability-helper";
import {
	StyleSheet,
	Text,
	View,
	WebView,
	ActionSheetIOS,
	UIManager,
	Button,
	ScrollView,
	StatusBar,
	Image,
	Slider,
	TouchableOpacity,
	Dimensions,
	ImageStore,
	CameraRoll,
	ImageEditor
} from "react-native";
import Expo, {Asset} from "expo";
// import {MaterialIcons} from "@exponent/vector-icons";
import Controls from "./Controls";
import Sliders from "./Sliders";
import SlidersTo from "./SlidersTo";
import SnapshotHack from "./SnapshotHack";
import AnimatedBlurEye from "./AnimatedBlurEye";

// const webapp = require('./webapp/index.html')
const webapp = require('./webapp/index.html')
// const webapp = require('./webapp/slider.html')
// const webapp = Asset.fromModule(require('./webapp/index.html')).downloadAsync()
// const jsPath = './webapp/static/js/main.txt'
// var jsPath = 'tst.txt'
// const jsAsTxt = require('./webapp/static/js/main.txt')
// const injectedJavaScript = Expo.Asset.fromModule(jsAsTxt).uri


const WIDTH = Dimensions.get('window').width
// const HEIGHT = Dimensions.get('window').height

class App extends React.Component {

	webview = null
	state = {
		base64: null,
		loading: true,
		image: {
			from: {
				r: null,
				g: null,
				b: null,
			},
			to: {
				r: null,
				g: null,
				b: null,
			},
		},
		numberOfImages: null,
		status: null,
		dataFromWebview: null,
		imageFromCam: null,
		stopSlidersAnimation: false,
		sliderContainerHeight: 69,
	}

	prettyPringJSON(o) {
		console.log('> pretty: ', JSON.stringify(o, null, 2))
	}

	async componentWillMount() {
		await Asset.fromModule(webapp).downloadAsync()

		// let img = new Image({source: require('./malmo-morning.jpg')})
		// this.prettyPringJSON(img)
		//
		// ImageStore.getBase64ForTag('./malmo-morning.jpg', res => console.log('> res, ', res),
		// 	res => console.log('> res, ', res))
	}

	async sendMockBase64() {
		let imgAsync = Asset.fromModule(require('./malmo-morning.jpg'))
		await imgAsync.downloadAsync()

		if (imgAsync.downloaded) {
			console.log('> imgAsync, ', imgAsync)

			const imageURL = imgAsync.localUri
			Image.getSize(imageURL, (width, height) => {
				var imageSize = {
					size: {
						width,
						height
					},
					offset: {
						x: 0,
						y: 0,
					},
				}

				ImageEditor.cropImage(imageURL, imageSize, (imageURI) => {
					console.log(imageURI);
					ImageStore.getBase64ForTag(imageURI, (base64Data) => {
						this.handleBase64(base64Data)
						ImageStore.removeImageForTag(imageURI);
					}, (reason) => console.log(reason) )
				}, (reason) => console.log(reason) )
			}, (reason) => console.log(reason))

		}
	}

	componentDidMount() {
		this.openCamera()
		// this.sendMockBase64()
		// this.sendMockBase64()
		// console.log('componentDidMount')
	}

	componentDidUpdate(prevProps, prevState) {
		if (this.state.numberOfImages === 0 && prevState.numberOfImages === 1) {
			this.setState({loading: true})
			this.openCamera()
		}

		if (this.state.numberOfImages > prevState.numberOfImages) {
			this.handleImageAdded()
		}
	}

	handleImageAdded = () => {
		console.log('> handleImageAdded')

		this.setState({
			image: this.adjustImageMessageFromWebView(),
			stopSlidersAnimation: false,
		}, () => {
			this.animateSlidersAfterNewImageAdded()
		})
	}


	getRandomLevel = () => (Math.random() * (0.4 - 0.1) + 0.1)

	animateSlidersAfterNewImageAdded() {
		var randomLevels = {
			r: this.getRandomLevel(),
			g: this.getRandomLevel(),
			b: this.getRandomLevel(),
		}

		console.log('> animateSlidersAfterNewImageAdded ', this.pretty(randomLevels))
		this.animateSlidersToLevels(randomLevels)
	}

	pretty = (o) => {
		return JSON.stringify(o, null, 2)
	}

	animateSlidersToLevels = (levels) => {
		setTimeout(() => {
			// console.log('> setTimeout')
			var moveMore = !this.state.stopSlidersAnimation
			if (moveMore) {
				var colors = Object.assign({}, this.state.image.to)
				moveMore = false
				Object.keys(colors).forEach(key => {
					if (colors[key] > levels[key]) {
						moveMore = true
						colors[key] = colors[key] - 0.01
						//TODO: hacky

						this.handleSliderColorChange({[key]: colors[key]}, 'to')
					}
				})

				let newState = update(this.state, {
					image: {to: {$set: colors}},
					stopSlidersAnimation: {$set: !moveMore},
				})
				this.setState(newState, () => {
					if (moveMore) this.animateSlidersToLevels(levels)
				})
			}
		}, 0.01)
	}

	adjustImageMessageFromWebView() {
		console.log('> adjustImageMessageFromWebView')

		this.handleSliderColorChange({r: 0}, 'from')
		this.handleSliderColorChange({g: 0}, 'from')
		this.handleSliderColorChange({b: 0}, 'from')

		this.handleSliderColorChange({r: 0.7}, 'to')
		this.handleSliderColorChange({g: 0.75}, 'to')
		this.handleSliderColorChange({b: 0.65}, 'to')

		let adjustedImageObj = {
			from: {
				r: 0,
				g: 0,
				b: 0,
			},
			to: {
				r: 0.7,
				g: 0.75,
				b: 0.65,
			},
		}

		return  adjustedImageObj

	}


	onMessage = e => {
		var data = JSON.parse(e.nativeEvent.data)

		let newState = Object.assign(data, {loading: false})
		console.log('> onMessage:', JSON.stringify(newState, null, 2))
		this.setState(newState)
		// this.setState({
		// 	dataFromWebview: JSON.stringify(data),
		// 	loading: false,
		// })

		//experimenting with getting the direct image share to work
		// if (Object.keys(data)[0] === 'base64') {
		//   ActionSheetIOS.showShareActionSheetWithOptions({
		//     url: data.base64,
		//     message: ' #thresh'
		//   },
		//   (error) => {
		//     alert(error)
		//     this.setState({base64: null})
		//   },
		//   (success, method) => {
		//     this.setState({base64: null})
		//   });
		// }
	}

	postDataToWebView = (message) => {
		if (this.webview) {
			let messageString = JSON.stringify(message)
			this.webview.postMessage(messageString)
			console.log('> postDataToWebView: ', messageString.substr(0, 128))
		}
	}

	handleImage = uri => {
		ActionSheetIOS.showShareActionSheetWithOptions({
				url: uri,
				message: ' #thresh'
			},
			(error) => {
				alert(error)
				this.setState({base64: null})
			},
			(success, method) => {
				this.setState({base64: null})
			});
	}

	handleSliderColorChange = (val, fromOrTo) => {
		var key = Object.keys(val)[0]
		var valWithFromTo = {
			color: key,
			val: Number(val[key]).toFixed(2),
			type: fromOrTo
		}

		var postData = {
			type: 'colorSliderChange',
			data: valWithFromTo,
		}
		this.postDataToWebView(postData)
	}

	openCamera = async() => {
		let result = await Expo.ImagePicker.launchCameraAsync({base64: true});

		if (!result.cancelled) {
			CameraRoll.saveToCameraRoll(this.getPrependedBase64(result.base64))
		}

		this.handleImageSelected(result)
	}

	getPrependedBase64 = (pureBase64) => {
		let base64WithPrefix = 'data:image/png;base64,' + pureBase64
		return base64WithPrefix
	}

	// Handle either new photo or selected image
	handleImageSelected = (result) => {
		// this.setState({loading: true})
		if (!result.cancelled) {
			// this.setState({loading: this.state.numberOfImages === 0 ? true : false}, () => {
				// console.log(JSON.stringify(result, null, 2));
				// console.log(JSON.stringify(Object.keys(result)))
				// this.setState({imageFromCam: result.uri})

				// let base64 = result.data
				// let base64WithPrefix = 'data:image/png;base64,' + base64

				this.handleBase64(result.base64)

				//TODO: redo
				// let numberOfImages = Number(this.state.numberOfImages)
				// this.setState({numberOfImages: ++numberOfImages})
			// })

		}

		if (result.cancelled) {
			this.setState({loading: false})
		}
	}

	handleBase64 = (base64) => {
		this.setState({loading: this.state.numberOfImages === 0 ? true : false}, () => {
			let base64WithPrefix = this.getPrependedBase64(base64)
			let data = {
				type: 'base64FromNative',
				data: base64WithPrefix,
			}
			this.postDataToWebView(data)
		})
	}

	onSliderMove = (val, fromOrTo) => {
		// this.setState(update(this.state, {
		// 	stopSlidersAnimation: {$set: true},
		// 	image: {$merge: val},
		// }),
		// 	() => this.handleSliderColorChange(val, 'to'))

		// this.setState(update(this.state, {
		// 		stopSlidersAnimation: {$set: true},
		// 		image: {$merge: val},
		// 	}))
		if (this.state.stopSlidersAnimation === false) {
			this.setState({stopSlidersAnimation: true})
		}
		this.handleSliderColorChange(val, fromOrTo)
	}

	// fuckShitBitch = () => {
	// 	console.log('=================> i: ', this.image)
	// }

	render() {

		const URL = 'https://i.ytimg.com/vi/n0CmqdivcaY/maxresdefault.jpg'

		return (
			<View style={styles.container}>
				<StatusBar hidden/>

				{/*{ !this.state.numberOfImages &&*/}
					{/*<Image*/}
						{/*ref={image => { this.image = image }}*/}
					{/*source={{ uri: URL }}*/}
					{/*onLoad={() => console.log('suckfuckfuck', this.image)}*/}
					{/*style={{ height: 140, width: 200 }}*/}
					{/*/>*/}
				{/*}*/}

				<View style={{
					width: WIDTH,
					height: WIDTH,
				}}>
					<WebView
						source={webapp}
						baseURL="/webapp"
						scalesPageToFit={true}
						ref={webview => {
							this.webview = webview;
						}}
						onMessage={this.onMessage}
						scrollEnabled={false}
					/>
				</View>
				<SnapshotHack base64={this.state.base64} callback={this.handleImage}/>

				{ this.state.loading &&
				<AnimatedBlurEye />
				}

				{ !this.state.loading &&
				<View style={{flex: 1}}>
					<Controls numberOfImages={this.state.numberOfImages}
							  handleImageSelected={this.handleImageSelected}
							  openCamera={this.openCamera}
							  onButtonPressed={(data) => {
								  {/*if (data.type === 'deleteImage' && this.state.numberOfImages===0)*/
								  }
								  this.postDataToWebView(data)
							  }}
					/>
					{ this.state.numberOfImages > 0 &&
					<ScrollView
						horizontal={false}
						pagingEnabled={true}
						bounces={false}
						style={styles.container}
						onLayout={e => this.setState({sliderContainerHeight: e.nativeEvent.layout.height})}
					>
						<SlidersTo style={{height: this.state.sliderContainerHeight}}
								   image={this.state.image.to}
								   handleChange={val => this.onSliderMove(val, 'to')}/>

						<Sliders style={{height: this.state.sliderContainerHeight}}
								 image={this.state.image.from}
								 handleChange={val => this.onSliderMove(val, 'from')}/>
					</ScrollView>
					}
				</View>
				}
			</View>
		);
	}
}

var styles = StyleSheet.create({
	container: {
		flex: 1,
		// backgroundColor: '#FAFAFA',
		flexDirection: 'column',
		// justifyContent: 'space-around',
		alignContent: 'center',
	},
	loginButton: {
		margin: 8,
		width: Dimensions.get('window').width,
		height: Dimensions.get('window').height,
	},
})

Expo.registerRootComponent(App);
