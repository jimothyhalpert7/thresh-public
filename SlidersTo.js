import React from "react";
import {View, Slider} from "react-native";
import {MaterialIcons} from "@expo/vector-icons";

const SETTINGS = {
	max: 1,
	step: 0.01,
}

class Sliders extends React.Component {

	constructor(props) {
		super(props)
		var colors = this.props.image
		this.state = {
			r: colors.r || 0,
			g: colors.g || 0,
			b: colors.b || 0
		}
	}

	handleChange(name, val) {
		// console.log(name, val);
		var deltaState = {}
		deltaState[name] = val.toFixed(2)
		if (this.props.handleChange) {
			this.props.handleChange(deltaState)
		}
	}

	getImagePropColorAsNumber(color) {
		return Number(this.props.image[color])
	}

	render() {
		return (
			<View style={this.props.style}>
				<Slider value={this.getImagePropColorAsNumber('r')} onValueChange={(v) => this.handleChange('r', v)}
						max={SETTINGS.max}
						step={SETTINGS.step}
						minimumTrackTintColor={'red'}
						className='red'/>
				<Slider value={this.getImagePropColorAsNumber('g')} onValueChange={(v) => this.handleChange('g', v)}
						max={SETTINGS.max}
						step={SETTINGS.step}
						minimumTrackTintColor={'#00DA3C'}/>
				<Slider value={this.getImagePropColorAsNumber('b')} onValueChange={(v) => this.handleChange('b', v)}
						max={SETTINGS.max}
						step={SETTINGS.step}
						minimumTrackTintColor={'#8AC7DE'}
						className='blue'/>
				<View style={{alignItems: 'center'}}>
					<MaterialIcons name={'keyboard-arrow-up'} size={32}/>
				</View>
			</View>
		)
	}
}

Sliders.defaultProps = {
	image: {
		r: null,
		g: null,
		b: null,
	},
	handleChange: null,
}

export default Sliders
