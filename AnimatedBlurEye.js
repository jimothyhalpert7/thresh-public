/**
 * Created by Alexander on 12-Mar-17.
 */
import React from "react";
import {Animated, Image, StyleSheet, View} from "react-native";
import { BlurView } from "expo";
import {MaterialIcons} from "@expo/vector-icons";

const AnimatedBlurView = Animated.createAnimatedComponent(BlurView);
class BlurViewExample extends React.Component {
	state = {
		intensity: new Animated.Value(0),
	}

	componentDidMount() {
		this._animate();
	}

	_animate = () => {
		let { intensity } = this.state;
		Animated.timing(intensity, {duration: 1000, toValue: 100}).start((value) => {
			Animated.timing(intensity, {duration: 1000, toValue: 0}).start(this._animate);
		});
	}

	render() {
		return (
			<View style={{flex: 1, padding: 50, alignItems: 'center', justifyContent: 'center'}}>
				<MaterialIcons name='remove-red-eye' size={32}/>

				<AnimatedBlurView
					tint="default"
					intensity={this.state.intensity}
					style={StyleSheet.absoluteFill} />
			</View>
		);
	}
}

export default BlurViewExample