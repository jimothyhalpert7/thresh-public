import React from "react";
import {View, TouchableOpacity, StyleSheet, Text, ScrollView, Dimensions} from "react-native";
import Expo from "expo";
import {MaterialIcons} from "@expo/vector-icons";

const CARD_MARGIN = 5;
// const CARD_WIDTH = Dimensions.get('window').width - CARD_MARGIN * 2;
const CARD_WIDTH = Dimensions.get('window').width
const BUTTON_SIZE = 32

class Controls extends React.Component {

	statics = {
		backgroundColors: [
			'#B80000',
			'#665C49',
			'#FCCB00',
			'#008B02',
			'#004DCF',
			'#5300EB',
			'#EB9694',
			'#FAD0C3',
			'#FEF3BD',
			'#C1E1C5',
			'#FDF3E7',
			'#3B3738',
			'#8C2318',
			'#A9B0B3',
			'#20293F',
			'#B80000',
			'#665C49',
			'#FCCB00',
			'#C07653',
			'#9D9D93',
			'#426070',
			'#FFFFFF',
			'#000000',
			'#EB9694',
			'#FAD0C3',
			'#FEF3BD',
			'#C1E1C5',
			'#FDF3E7',
			'#3B3738',
			'#8C2318',
			'#A9B0B3',
			'#20293F',
			'#226F49',
			'#15305B',
			'#F7A471',
			'#E8DDCB',
			'#CDB380',
			'#036564',
			'#033649',
			'#031634',
			'#E5D95E',
			'#F5AD7A',
			'#E35F94',
			'#883991',
			'#332839',
			'#452632',
			'#91204D',
			'#E4844A',
			'#E8BF56',
			'#E2F7CE',
			'#8A8780',
			'#E47267',
			'#66245B',
			'#616382',
			'#FF3366',
			'#8F4D65',
		],
	}

	constructor(props) {
		super(props);
		let colors = this.getArrayOfRandomColors()
		this.state = {
			page: 0,
			colors: colors,
			color: colors[0],
			status: null,
		}
	}

	componentDidMount() {
		this.onPressSendAction('colorSelect', this.state.color)
	}

	getArrayOfRandomColors = () =>
		this.statics.backgroundColors
			.sort(() => 0.5 - Math.random()).slice(0, 39)

	nextControlsPage = () => {
		var page = (this.state.page + 1) % 3
		this.setState({page: page})
	}

	onPressSendAction(type, data) {
		var dataToSend = {
			type: type,
			data: data,
		}
		this.setState({status: dataToSend})
		this.props.onButtonPressed(dataToSend)
	}

	createImageSelectionButtons(numberOfImages) {

		var numberIcons = [
			'looks-one',
			'looks-two',
			'looks-3',
			'looks-4',
			'looks-5',
		]

		return Array(numberOfImages).fill().map((_, i) => {
			var iconId = i < (numberIcons.length - 1) ? i : (numberIcons.length - 1)
			// console.log('> iconId, ', iconId)
			return (
				<TouchableOpacity key={iconId}
								  style={controlStyles.button}
								  onPress={ () => this.onPressSendAction('imageSelect', iconId)}>
					<MaterialIcons name={numberIcons[iconId]} size={32}/>
				</TouchableOpacity>
			)
		})
	}

	createImageActionButtons(iconsActionMap) {
		return Object.keys(iconsActionMap).map(key => {
			return (
				<TouchableOpacity
					key={key}
					style={controlStyles.button}
					onPress={() =>
						this.onPressSendAction(iconsActionMap[key]) }>
					<MaterialIcons name={key} size={BUTTON_SIZE}/>
				</TouchableOpacity>
			)
		})
	}

	createBackgroundColorButtons(backgroundColors) {
		return <View style={{
			flex: 1,
			flexDirection: 'row',
			flexWrap: 'wrap',
		}}>
			{
				this.state.colors.map((color, i) => {
					return (
						<TouchableOpacity
							style={ StyleSheet.flatten([
								controlStyles.colorSquare,
								{
									backgroundColor: color
								}])} key={i}
							onPress={() => {
								this.setState({colors: this.getArrayOfRandomColors()})
								this.onPressSendAction('colorSelect', color)}
							}/>
					)
				})
			}
		</View>
	}

	openLibrary = async () => {
		let result = await Expo.ImagePicker.launchImageLibraryAsync({base64: true});
		this.props.handleImageSelected(result)
	}

	render() {
		var iconsActionMap = {
			'content-cut': 'deleteImage',
			// 'rotate-left': 'rotateLeft',
			'rotate-right': 'rotateRight',
			'share': 'share',
			// 'remove-red-eye': 'open',
		}

		return (
			<ScrollView
				style={styles.container}
				contentContainerStyle={styles.content}
				horizontal={true}
				pagingEnabled={true}
				bounces={false}
			>
				<View style={[styles.card]}>
					<View style={styles.pageSection}>
						{ this.props.numberOfImages > 0 &&
							this.createImageActionButtons(iconsActionMap)
						}

						<TouchableOpacity
							onPress={this.openLibrary}
							style={controlStyles.button}>
							<MaterialIcons name='photo-library' size={32}/>
						</TouchableOpacity>

						<TouchableOpacity
							onPress={this.props.openCamera}
							style={controlStyles.button}>
							<MaterialIcons name='remove-red-eye' size={32}/>
						</TouchableOpacity>
					</View>
					{ this.props.numberOfImages > 0 &&
					<View style={styles.pageSection}>
						{
							this.createImageSelectionButtons(this.props.numberOfImages)
						}
					</View>
					}
				</View>
				{ this.props.numberOfImages > 0 &&
				<View style={styles.card}>
					<View style={styles.pageSection}>
						{ this.createBackgroundColorButtons(this.statics.backgroundColors) }
					</View>
				</View>
				}
			</ScrollView>
		)
	}
}

// <View style={controlStyles.controlPage}>
const controlStyles = StyleSheet.create({
	colorSquare: {
		height: 32,
		width: 32,
	},
	button: {
		height: 56,
		width: 56,
		// backgroundColor: 'yellow',
		alignItems: 'center',
		justifyContent: 'center',
	}
})


var styles = StyleSheet.create({
	container: {
		flex: 1,
		// marginVertical: 24,
		// backgroundColor: 'red',
	},
	content: {
		alignItems: 'flex-start',
		// backgroundColor: 'green',
	},
	card: {
		flex: 1,
		// backgroundColor: 'blue',
		width: CARD_WIDTH,
		flexDirection: 'column',
		// justifyContent: 'space-around',
		alignItems: 'center',
	},
	pageSection: {
		flex: 1,
		justifyContent: 'space-around',
		width: CARD_WIDTH,
		flexDirection: 'row',
		flexWrap: 'wrap',
	}
});

export default Controls